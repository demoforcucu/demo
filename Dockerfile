FROM markhobson/maven-chrome

MAINTAINER Mustafa Güler <mustafa.glr97@gmail.com>

COPY . .

CMD mvn -Dtest=CucumberRunner -DfailIfNoTests=false test
