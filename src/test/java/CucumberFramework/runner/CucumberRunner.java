package CucumberFramework.runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import junit.framework.Test;
@RunWith(Cucumber.class)
@CucumberOptions(

	features= {"src/test/java/CucumberFramework/featurefiles/"},
	glue = {"CucumberFramework.stepDefinition"},
	plugin = {"pretty","html:terget/cucumber"}
	
)
public class CucumberRunner {

}
