@tag
Feature: Title of your feature
  Scenario: Create Customer
    Given I navigate to "https://www.spotify.com/tr/signup/" page
    When I fill textboxes 
    	| register-email   |    asdasd@gmail.com      |
    	| register-confirm-email   |    asdasd@gmail.com       |
    	| register-password   |    asdasd       |
    	| register-displayname   |    mustafa       |
    	| register-dob-day   |    06      |
    	| register-dob-year   |    1997       |
    And I fill checkboxes 
    	|  register-thirdparty  | 
    And I fill comboboxes 
    	| register-dob-month   |     Ağustos      |
    And I click radioboxes 
    	| register-male   |
    And I click buttons 
    	| register-button-email-submit   | 
    Then should be logged in
    