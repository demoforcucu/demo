package CucumberFramework.stepDefinition;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class Steps {
	WebDriver driver;
	Actions actions;
	java.util.List<Map<String, String>> list;
	
	@Given("^I navigate to \"(.*)\" page$")
	public void navigate_page(String page){

		try {
	        System.setProperty("webdriver.chrome.driver", "/usr/bin/chromedriver");
	        ChromeOptions options = new ChromeOptions();
	        options.addArguments("--headless");
	        options.addArguments("--no-sandbox");
	        options.addArguments("--disable-dev-shm-usage");
	        options.setExperimentalOption("useAutomationExtension", false);
	        driver=new ChromeDriver(options);
			driver.get(page);
			actions=new Actions(driver);
		} catch (Exception e) {
			throw new RuntimeException("Exception while filling " + page, e);
		}
		
		
	}
	
	@When("^I fill textboxes$")
	public void fill_textbox(Map<String, String> dataMap) {
	    for (Map.Entry<String, String> item : dataMap.entrySet()) {
	        try {
	            WebElement element = null;
	            if (driver.findElements(By.id(item.getKey())).size() != 0)
	                element = driver.findElement(By.id(item.getKey()));
	            else if (driver.findElements(By.xpath(item.getKey())).size() != 0)
	                element = driver.findElement(By.xpath(item.getKey()));
	            else if (driver.findElements(By.cssSelector(item.getKey())).size() != 0)
	                element = driver.findElement(By.cssSelector(item.getKey()));
	            element.click();
	            element.sendKeys(item.getValue());
	        } catch (Exception e) {
	            throw new RuntimeException("Exception while filling " + item.getKey(), e);
	        }
	    }
	}
	@And("^I fill checkboxes$")
	public void fill_checkbox(List<String> dataList) {
	    for (String item : dataList) {
	        try {
	            WebElement element = null;
	            if (driver.findElements(By.id(item)).size() != 0)
	                element = driver.findElement(By.id(item));
	            else if (driver.findElements(By.xpath(item)).size() != 0)
	                element = driver.findElement(By.xpath(item));
	            else if (driver.findElements(By.cssSelector(item)).size() != 0)
	                element = driver.findElement(By.cssSelector(item));
	            actions.moveToElement(element).click().perform();
	        } catch (Exception e) {
	            throw new RuntimeException("Exception while filling " + item, e);
	        }
	    }
	}
	@And("^I fill comboboxes$")
	public void fill_combobox(Map<String, String> dataMap) {
	    for (Map.Entry<String, String> item : dataMap.entrySet()) {
	        try {
	            WebElement element = null;
	            if (driver.findElements(By.id(item.getKey())).size() != 0)
	                element = driver.findElement(By.id(item.getKey()));
	            else if (driver.findElements(By.xpath(item.getKey())).size() != 0)
	                element = driver.findElement(By.xpath(item.getKey()));
	            else if (driver.findElements(By.cssSelector(item.getKey())).size() != 0)
	                element = driver.findElement(By.cssSelector(item.getKey()));
	            new Select(element).selectByVisibleText(item.getValue());
	        } catch (Exception e) {
	            throw new RuntimeException("Exception while filling " + item.getKey(), e);
	        }
	    }
	}
	@And("^I click buttons$")
	public void click_button(List<String> dataList) {
	    for (String item : dataList) {
	        try {
	            WebElement element = null;
	            if (driver.findElements(By.id(item)).size() != 0)
	                element = driver.findElement(By.id(item));
	            else if (driver.findElements(By.xpath(item)).size() != 0)
	                element = driver.findElement(By.xpath(item));
	            else if (driver.findElements(By.cssSelector(item)).size() != 0)
	                element = driver.findElement(By.cssSelector(item));
	            actions.moveToElement(element).click().perform();
	        } catch (Exception e) {
	            throw new RuntimeException("Exception while filling " + item, e);
	        }
	    }
	}
	@And("^I click radioboxes$")
	public void click_radiobox(List<String> dataList) {
	    for (String item : dataList) {
	        try {
	            WebElement element = null;
	            if (driver.findElements(By.id(item)).size() != 0)
	                element = driver.findElement(By.id(item));
	            else if (driver.findElements(By.xpath(item)).size() != 0)
	                element = driver.findElement(By.xpath(item));
	            else if (driver.findElements(By.cssSelector(item)).size() != 0)
	                element = driver.findElement(By.cssSelector(item));
	            actions.moveToElement(element).click().perform();
	        } catch (Exception e) {
	            throw new RuntimeException("Exception while filling " + item, e);
	        }
	    }
	}	
	
	
	@Then("should be logged in$")
	public void logged() throws Throwable {
		System.out.print("Success");
	}
}
