$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("scenario.feature");
formatter.feature({
  "line": 2,
  "name": "Title of your feature",
  "description": "",
  "id": "title-of-your-feature",
  "keyword": "Feature",
  "tags": [
    {
      "line": 1,
      "name": "@tag"
    }
  ]
});
formatter.scenario({
  "line": 3,
  "name": "Create Customer",
  "description": "",
  "id": "title-of-your-feature;create-customer",
  "type": "scenario",
  "keyword": "Scenario"
});
formatter.step({
  "line": 4,
  "name": "I navigate to \"https://www.spotify.com/tr/signup/\" page",
  "keyword": "Given "
});
formatter.step({
  "line": 5,
  "name": "I fill textboxes",
  "rows": [
    {
      "cells": [
        "register-email",
        "asdasd@gmail.com"
      ],
      "line": 6
    },
    {
      "cells": [
        "register-confirm-email",
        "asdasd@gmail.com"
      ],
      "line": 7
    },
    {
      "cells": [
        "register-password",
        "asdasd"
      ],
      "line": 8
    },
    {
      "cells": [
        "register-displayname",
        "mustafa"
      ],
      "line": 9
    },
    {
      "cells": [
        "register-dob-day",
        "06"
      ],
      "line": 10
    },
    {
      "cells": [
        "register-dob-year",
        "1997"
      ],
      "line": 11
    }
  ],
  "keyword": "When "
});
formatter.step({
  "line": 12,
  "name": "I fill checkboxes",
  "rows": [
    {
      "cells": [
        "register-thirdparty"
      ],
      "line": 13
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I fill comboboxes",
  "rows": [
    {
      "cells": [
        "register-dob-month",
        "Ağustos"
      ],
      "line": 15
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 16,
  "name": "I click radioboxes",
  "rows": [
    {
      "cells": [
        "register-male"
      ],
      "line": 17
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 18,
  "name": "I click buttons",
  "rows": [
    {
      "cells": [
        "register-button-email-submit"
      ],
      "line": 19
    }
  ],
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "should be logged in",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "https://www.spotify.com/tr/signup/",
      "offset": 15
    }
  ],
  "location": "Steps.navigate_page(String)"
});
formatter.result({
  "duration": 5800463336,
  "status": "passed"
});
formatter.match({
  "location": "Steps.fill_textbox(String,String\u003e)"
});
formatter.result({
  "duration": 903337532,
  "status": "passed"
});
formatter.match({
  "location": "Steps.fill_checkbox(String\u003e)"
});
formatter.result({
  "duration": 173976508,
  "status": "passed"
});
formatter.match({
  "location": "Steps.fill_combobox(String,String\u003e)"
});
formatter.result({
  "duration": 103579142,
  "status": "passed"
});
formatter.match({
  "location": "Steps.click_radiobox(String\u003e)"
});
formatter.result({
  "duration": 159938475,
  "status": "passed"
});
formatter.match({
  "location": "Steps.click_button(String\u003e)"
});
formatter.result({
  "duration": 189373312,
  "status": "passed"
});
formatter.match({
  "location": "Steps.logged()"
});
formatter.result({
  "duration": 366437,
  "status": "passed"
});
});